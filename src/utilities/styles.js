import { StyleSheet } from 'react-native';

export default styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f6fbff',
  },
  header: {
    flex: 0.8,
    flexDirection: 'row'
  },
  welcome: {
    fontSize: 18,
    textAlign: 'center',
  },
  welcome: {
    fontSize: 18,
    textAlign: 'center',
  },
  modalMovie: {
    fontSize: 18,
    textAlign: 'center',
    color: 'white',
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  taps: {
    paddingVertical: 40,
    justifyContent: 'center',
    alignItems: 'center',
    margin: 10,
    backgroundColor: '#ddd',
  },
  seat1B: {
    width: 16,
    height: 16,
    margin: 1
  },
  seat2B: {
    width: 16,
    height: 16,
    margin: 1
  },
  seat3B: {
    width: 30,
    height: 10,
  },
  screen: {
    width: '100%',
    height: 50,
    borderRadius: 20,
}
});  