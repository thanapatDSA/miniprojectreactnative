import React, { Component } from 'react';
import { Image, FlatList, Text, View, Alert, ImageBackground, TouchableOpacity, ScrollView } from 'react-native';
import { connect } from 'react-redux'
import styles from '../utilities/styles'
import NavigationBar from 'react-native-navbar';
import { Button, Icon } from '@ant-design/react-native';
import { push } from 'connected-react-router'
import { CheckBox } from 'react-native-elements'
import SOFA from '../images/seat3.png'
import SOFA_CHECKED from '../images/seat3_check.png'


class seatSelect extends Component {
  state = {
    movie: [],
    seats: [],
    sofa: [],
    premium: [],
    deluxe: [],
    Checked: false,
    booking: [],
    new: []
  }
  UNSAFE_componentWillMount() {
    console.log('Prop movie:', this.props.location);
    this.setState({ movie: this.props.location.state.movie, seats: this.props.location.state.movie.seats })
    this.setState({ sofa: this.props.location.state.movie.seats[0], premium: this.props.location.state.movie.seats[1], deluxe: this.props.location.state.movie.seats[2] })

  }

  componentDidMount() {
    this.renderSeat();
  }
  addZero(time) {
    if (time < 10) {
      time = "0" + time;
    }
    return time;
  }

  renderImage = (value) => {
    var imgSource = value ? SOFA_CHECKED : SOFA;
    return (
      <View>
        <Image
          style={styles.seat3B}
          source={imgSource}
        />
      </View>
    );
  }

  goToSummary = () => {
    this.props.history.push('./summary', { movie: this.state.movie, seats: this.state.booking })
  }
  seatCheck = (row, col, type) => {
    console.log("booking:", this.state.booking);

    const check = this.state.booking.find(each => {
      if (row === each.row && col === each.column && type === each.type) {
        return true
      } else {
        return false
      }
    })

    if (check === '') {
      return false
    } else {
      return true
    }
  }

  bookingChack = (row, col, type, item) => {
    if (item === 'F') {
      this.setState({
        booking: [...this.state.booking, {
          type: type,
          row: row,
          column: col
        }]
      })
    } else if (item === 'C') {
      const result = this.state.booking.filter((item) => {
        return !(item.row === row && item.column === col && item.type === type)
      })
      console.log('resultbookingChack:', result);
      this.setState({ booking: result })
    }
  }

  renderSeat = () => {
    console.log('seats:', this.state.seats);

    let item = this.state.seats
    item = item.map(i => {
      return Object.keys(i).reduce((sum, each) => {
        sum[each] = i[each]
        if (each.includes('row') && !each.includes('rows')) {
          sum[each] = sum[each].map(item => item === true ? 'B' : 'F')
          console.log();
        }
        return sum
      }, {})
    })
    console.log('POOM2: ', item);
    this.setState({ new: item })
  }

  onPressSeat = (type, row, column, itemSeat) => {
    this.bookingChack(column + 1, row + 1, type, itemSeat)
    let item = this.state.new
    item = item.map(i => {
      if (i.type !== type) {
        return i
      }
      return Object.keys(i).reduce((sum, each, indexRows) => {
        sum[each] = i[each]
        if (each.includes('row') && !each.includes('rows')) {
          sum[each] = sum[each].map((item, indexCulumn) => {
            if (each === `row${(row + 1)}` && indexCulumn === column && item === 'F') {
              return 'C'
            }
            else {
              if (each === `row${(row + 1)}` && indexCulumn === column && item === 'C') {
                return 'F'
              } else {
                return item
              }
            }
          })
        }
        return sum
      }, {})
    })
    console.log('POOM2: ', item);
    this.setState({ new: item })
    console.log('Booking,:', this.state.booking)
  }
  _keyExtractorSeats = (item, index) => item._id

  _keyExtractorDetailSeat = (item, index) => item._id
  render() {

    console.log('booking:', this.state.booking);
    console.log('State Movie:', this.state.movie)
    console.log('State Seate:', this.state.seats)

    return (
      <View style={styles.container}>
        <View style={styles.header} >
          <View style={{ flex: 1, }}>
            <NavigationBar title={{ title: 'SeatSelect' }}
              leftButton={{
                title: 'Back',
                handler: () => this.props.history.push('./menu', { index: 0 })
              }} />
          </View>
        </View>
        <View style={{
          flex: 11, flexDirection: 'column',
          backgroundColor: '#b9b9b9',
          justifyContent: 'center',
        }}>
          <ImageBackground
            blurRadius={5}
            source={{ uri: this.state.movie.movie.image }}
            style={{
              flex: 1,
              flexDirection: 'row',
              height: 200,
              width: '100%',
              justifyContent: 'center',
            }}
          >
            <View style={{
              flex: 1.5,
              alignItems: 'center',
              marginTop: 10,
            }}>
              <Image
                source={{ uri: this.state.movie.movie.image }}
                style={{ width: '100%', height: 180, resizeMode: 'contain' }}
              />
            </View>
            <View style={{ flex: 2, marginTop: 50, alignItems: 'center' }}>
              <Text style={{
                textAlign: 'center',
                fontSize: 18,
                color: 'white',
              }}>
                {this.state.movie.movie.name}
              </Text>
              <Text style={{
                textAlign: 'center',
                fontSize: 15,
                color: 'white',
              }}>
                <Icon name="clock-circle" size="xxs" color="white" /> {this.state.movie.movie.duration} min
              </Text>
              <Text style={{
                textAlign: 'center',
                fontSize: 15,
                color: 'white',
              }}>
                {this.addZero(new Date(this.state.movie.startDateTime).getHours())}
                :{this.addZero(new Date(this.state.movie.startDateTime).getMinutes())} - {this.addZero(new Date(this.state.movie.endDateTime).getHours())}
                :{this.addZero(new Date(this.state.movie.endDateTime).getMinutes())}
              </Text>
              <Text style={{
                textAlign: 'center',
                fontSize: 15,
                color: 'white',
              }}>{new Date(this.state.movie.startDateTime).toLocaleDateString()}</Text>
            </View>
          </ImageBackground>
          <View style={{
            flex: 1.85,
            flexDirection: 'row',
          }}>
            <ScrollView>
              <View style={{
                flex: 1,
                margin: 20,
                alignItems: 'center',
                justifyContent: 'center',
              }}>
                <Image style={styles.screen} source={require('../images/screen2.png')} />
                {
                  this.state.new.map((i, index) => {
                    return Object.keys(i).map((rowsData, indexRows) => {
                      return (
                        <FlatList
                          keyExtractor={this._keyExtractorSeats}
                          data={i['row' + (indexRows + 1)]}
                          numColumns={i.columns}
                          extraData={this.state}
                          renderItem={({ item, index }) => {
                            console.log('eieiei', i.type)
                            if (item === 'F') {
                              if (i.type === "SOFA") {
                                return (
                                  <TouchableOpacity onPress={() => { this.onPressSeat(i.type, indexRows, index, item) }}>
                                    <Image style={styles.seat3B} source={require('../images/seat3.png')} />
                                  </TouchableOpacity>
                                )
                              }
                              else if (i.type === "PREMIUM") {
                                return (
                                  <TouchableOpacity onPress={() => { this.onPressSeat(i.type, indexRows, index, item) }}>
                                    <Image style={styles.seat1B} source={require('../images/seat2.png')} />
                                  </TouchableOpacity>
                                )
                              }
                              else if (i.type === "DELUXE") {
                                return (
                                  <TouchableOpacity onPress={() => { this.onPressSeat(i.type, indexRows, index, item) }}>
                                    <Image style={styles.seat1B} source={require('../images/seat1.png')} />
                                  </TouchableOpacity>
                                )
                              }
                            }
                            else if (item === 'C') {
                              if (i.type === "SOFA") {
                                return (
                                  <TouchableOpacity onPress={() => { this.onPressSeat(i.type, indexRows, index, item) }}>
                                    <Image style={styles.seat3B} source={require('../images/seat3_check.png')} />
                                  </TouchableOpacity>
                                )
                              }
                              else if (i.type === "PREMIUM") {
                                return (
                                  <TouchableOpacity onPress={() => { this.onPressSeat(i.type, indexRows, index, item) }}>
                                    <Image style={styles.seat2B} source={require('../images/seat2_check.png')} />
                                  </TouchableOpacity>
                                )
                              }
                              else if (i.type === "DELUXE") {
                                return (
                                  <TouchableOpacity onPress={() => { this.onPressSeat(i.type, indexRows, index, item) }}>
                                    <Image style={styles.seat1B} source={require('../images/seat1_check.png')} />
                                  </TouchableOpacity>
                                )
                              }
                            }
                            else if (item === 'B') {
                              if (i.type === "SOFA") {
                                return <Image style={styles.seat3B} source={require('../images/seat3_true.png')} />
                              }
                              else if (i.type === "PREMIUM") {
                                return <Image style={styles.seat2B} source={require('../images/seat2_true.png')} />
                              }
                              else if (i.type === "DELUXE") {
                                return <Image style={styles.seat1B} source={require('../images/seat1_true.png')} />
                              }
                              
                            }
                          }
                          }
                        />
                      )
                    })
                  }).reverse()
                }
              </View>
            </ScrollView>
          </View>
        </View>
        <View>
          <Button type='primary' onPress={() => {
            if (this.state.booking === []) {
              alert('Plase select seat')
            } else {
              this.goToSummary()
            }
           }}>Booking</Button>
        </View>
      </View>
    )
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    push: location => {
      dispatch(push(location))
    }
  }
}
export default connect(null, { push })(seatSelect)