import React, { Component } from 'react';
import { Image, StyleSheet, Text, View, Alert, TextInput, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux'
import styles from '../utilities/styles'
import NavigationBar from 'react-native-navbar';
import { Icon, Button, InputItem, WhiteSpace, List } from '@ant-design/react-native';
import axios from 'axios'
class editProfile extends Component {
  state = {
    firstName: '',
    lastName: '',
    oldPassword: '',
    newPassword: '',
    confrimPassword: '',
    isShowChangePass: true,
  }
  onClickChangeProfile = (firstName, lastName) => {
    const { profile } = this.props
    console.log('TOKEN',this.props.profile[profile.length-1].token);
    
    if (firstName === '') {
      alert("First Name can't be blank")
    } else if (lastName === '') {
      alert("Last Name can't be blank")
    } else {
      axios({
        method: 'put',
        url: 'https://zenon.onthewifi.com/ticGo/users',
        headers: { Authorization: `Bearer ${this.props.profile[profile.length-1].token}` },
        data: {
          firstName: firstName,
          lastName: lastName
        }
      })
        .then(res => {
          const { data } = res
          const { user } = data
          console.log("RES:", res);
          this.props.history.push('/menu', { index: 2 })
        }).catch(error => {
          console.log("ERROR:", error);
        })
    }
  }
  onClickChangePassword = (oldPassword, newPassword, confrimPassword) => {
    const { profile } = this.props
    if (oldPassword === '') {
      alert("Old Password can't be blank")
    } else if (newPassword === '') {
      alert("New Passwordcan't be blank")
    } else if (confrimPassword === '') {
      alert("Confrim Passwordcan't be blank")
    } else if (newPassword !== confrimPassword) {
      alert('New Password does not match')
    }
    else {
      axios({
        method: 'put',
        url: 'https://zenon.onthewifi.com/ticGo/users/password',
        headers: { Authorization: `Bearer ${this.props.profile[profile.length-1].token}` },
        data: {
          oldPassword: oldPassword,
          newPassword: newPassword
        }
      })
        .then(res => {
          const { data } = res
          const { user } = data
          console.log("RES:", res);
          this.props.history.push('/menu', { index: 2 })
        }).catch(error => {
          console.log("ERROR:", error);
        })
    }
  }
  goToProfile = () => {
    this.props.history.push('/menu', { index: 2 })
  }
  showChangePass = (isShowChangePass) => {
    this.setState({ isShowChangePass: !isShowChangePass })
  }
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.header} >
          <View style={{ flex: 1, }}>
            <NavigationBar title={{ title: 'EditProfile' }}
              leftButton={{
                title: 'Back',
                handler: () => this.goToProfile()
              }} />
          </View>
        </View>
        <WhiteSpace />
        <View style={{ flex: 7, alignItems: 'center' }}>
          <View >
            <List renderHeader={'Edit Profile'}>
              <InputItem placeholder='First Name'
                clear
                labelNumber={2}
                type="primary"
                style={{}}
                onChangeText={(value) => { this.setState({ firstName: value }) }}><Icon name={'user'} /></InputItem>
              <WhiteSpace />
              <InputItem placeholder='Last Name'
                clear
                labelNumber={2}
                type="primary"
                style={{ height: 40 }}
                onChangeText={(value) => { this.setState({ lastName: value }) }} > </InputItem>
              <WhiteSpace />
              <Button onPress={() => { this.onClickChangeProfile(this.state.firstName, this.state.lastName) }}
                type="primary"
                style={{ width: 300, height: 40 }} >Save Profile</Button>
              <WhiteSpace />
            </List>
            <List renderHeader={'Change Password'}>
              <InputItem placeholder='Old Password'
                clear
                disabled={this.state.isShowChangePass}
                labelNumber={2}
                type="password"
                style={{}}
                onChangeText={(value) => { this.setState({ oldPassword: value }) }}><Icon name={'lock'} /></InputItem>
              <WhiteSpace />
              <InputItem placeholder='New Password'
                clear
                disabled={this.state.isShowChangePass}
                labelNumber={2}
                type="password"
                style={{ height: 40 }}
                onChangeText={(value) => { this.setState({ newPassword: value }) }} ><Icon name={'lock'} /></InputItem>
              <InputItem placeholder='Confrim Password'
                clear
                disabled={this.state.isShowChangePass}
                labelNumber={1.7}
                type="password"
                style={{ height: 40 }}
                onChangeText={(value) => { this.setState({ confrimPassword: value }) }} >  </InputItem>
              <WhiteSpace />
              <View style={{ flexDirection: 'row' }}>
                <View>
                  <Button onPress={() => { this.showChangePass(this.state.isShowChangePass) }}
                    disabled={!this.state.isShowChangePass}
                    type="warning"
                    style={{ width: 150, height: 40 }} >Change</Button>
                  <WhiteSpace />
                </View>
                <View>
                  <Button onPress={() => { this.showChangePass(this.state.isShowChangePass) }}
                    disabled={this.state.isShowChangePass}
                    type="primary"
                    style={{ width: 150, height: 40 }} >Cancel</Button>
                  <WhiteSpace />
                </View>
              </View>
              <Button onPress={() => { this.onClickChangePassword(this.state.oldPassword, this.state.newPassword, this.state.confrimPassword) }}
                disabled={this.state.isShowChangePass}
                type="primary"
                style={{ width: 300, height: 40 }} >Save Password</Button>
              <WhiteSpace />
            </List>
          </View>
        </View>
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    profile: state.profile
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    editProfile: (email, firstname, lastname) => {
      dispatch({
        type: 'EDIT_PROFILE',
        email: email,
        firstname: firstname,
        lastname: lastname
      })
    },
    push: location => {
      dispatch(push(location))
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(editProfile)