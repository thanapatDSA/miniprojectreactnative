import React, { Component } from 'react';
import { Image, ImageBackground, Text, View, Alert, FlatList, TouchableOpacity, Modal, ScrollView } from 'react-native';
import { connect } from 'react-redux'
import styles from '../utilities/styles'
import NavigationBar from 'react-native-navbar';
import axios from 'axios'
import { NavBar, Icon, Card, Button, WingBlank, WhiteSpace, Tabs } from '@ant-design/react-native';
import { push } from 'connected-react-router'

var options = { weekday: 'short', month: 'short', day: 'numeric' }
var tab1 = Date.now()
var tab2 = new Date(new Date().getTime() + 24 * 60 * 60 * 1000)
tab2 = tab2.getTime()
var tab3 = new Date(new Date().getTime() + 48 * 60 * 60 * 1000)
tab3 = tab3.getTime()


class listMovie extends Component {
  state = {
    movie: [],
    movies: [],
    modalVisible: false,
    movieShow1: [],
    movieShow2: [],
    movieShow3: [],
    tabs: [
      { title: `${new Date().toLocaleDateString("en-US", options)}` },
      { title: `${new Date(new Date().getTime() + 24 * 60 * 60 * 1000).toLocaleDateString("en-US", options)}` },
      { title: `${new Date(new Date().getTime() + 48 * 60 * 60 * 1000).toLocaleDateString("en-US", options)}` },
    ]
  }
  UNSAFE_componentWillMount() {
    axios.get('https://zenon.onthewifi.com/ticGo/movies')
      .then(response => response.data)
      .then(data => {
        this.setState({ movies: data })
      })

  }

  getTimeMovie1 = (id) => {
    console.log(new Date());
    axios.get(`https://zenon.onthewifi.com/ticGo/movies/showtimes/movie/${id}`, {
      params: { date: tab1 }
    })
      .then(response => response.data)
      .then(data => {
        this.setState({ movieShow1: data })
      })
  }

  getTimeMovie2 = (id) => {
    axios.get(`https://zenon.onthewifi.com/ticGo/movies/showtimes/movie/${id}`, {
      params: { date: tab2 }
    })
      .then(response => response.data)
      .then(data => {
        this.setState({ movieShow2: data })
      })
  }

  getTimeMovie3 = (id) => {
    console.log(new Date());
    axios.get(`https://zenon.onthewifi.com/ticGo/movies/showtimes/movie/${id}`, {
      params: { date: tab3 }
    })
      .then(response => response.data)
      .then(data => {
        this.setState({ movieShow3: data })
      })
  }

  addZero(time) {
    if (time < 10) {
      time = "0" + time;
    }
    return time;
  }
  openMovie = (movie, movieID) => {
    this.setState({
      modalVisible: true,
      movie: movie,
    })
    this.getTimeMovie1(movieID)
    this.getTimeMovie2(movieID)
    this.getTimeMovie3(movieID)
  }

  closeMovie = () => {
    this.setState({
      modalVisible: false,
      movie: [],
      movieShow1: [],
      movieShow2: [],
      movieShow3: [],
    });
  }
  goToBooking = (item) => {
    console.log('gotoBookItem:', item);
    const movie = item
    this.props.push('/seatSelect', { movie: item })
  }
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.header} >
          <View style={{ flex: 1 }}>
            <NavigationBar title={{ title: 'Movie' }} />
          </View>
        </View>
        <View style={{ flex: 11, alignItems: 'center' }}>
          <FlatList
            data={this.state.movies}
            numColumns={2}
            renderItem={({ item }) =>
              <WingBlank size="md">
                <Card style={{ width: 172, height: 290 }}>
                  <View style={{ flexDirection: 'column' }}>
                    <TouchableOpacity onPress={() => { this.openMovie(item, item._id) }}>
                      <Image style={{ width: 170, height: 252 }}
                        source={{ uri: item.image }} />
                    </TouchableOpacity>
                    <View style={{ justifyContent: 'center', alignItems: 'center', }}>
                      <Text style={{ textAlign: 'center' }}>{item.name}</Text>
                    </View>
                  </View>
                </Card>
                <WhiteSpace size="md" />
              </WingBlank>
            }
          />
        </View>
        <View>
        </View>
        <Modal
          animationType="slide"
          transparent={false}
          visible={this.state.modalVisible}
          onRequestClose={() => {
            Alert.alert('Modal has been closed.');
          }}>
          <View style={{ flex: 1 }}>
            <View style={{ flex: 1 }} >
              <NavigationBar title={{ title: `${this.state.movie.name}` }}
                leftButton={{
                  title: 'Back',
                  handler: () => this.closeMovie()
                }}
                statusBar={{ showAnimation: 'slide', tintColor: 'black' }} />
            </View>
            <View style={{ flex: 13, flexDirection: 'column' }}>
              <ImageBackground
                blurRadius={5}
                source={{ uri: this.state.movie.image }}
                style={{ alignItems: 'center', justifyContent: 'center' }}>
                <Image style={{ width: 180, height: 266 , marginTop:10}}
                  source={{ uri: this.state.movie.image }} />
                <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                  <WingBlank>
                    <WhiteSpace size="sm" />
                    <Text style={styles.modalMovie}><Icon name="clock-circle" size="xxs" color="white" /> {this.state.movie.duration} min.</Text>
                    <WhiteSpace size="sm" />
                  </WingBlank>
                </View>
              </ImageBackground>
              <Tabs tabs={this.state.tabs} tabBarPosition="top">
                <View style={{ flex: 5, alignItems: 'center' }}>
                  <WhiteSpace />
                  <FlatList
                    data={this.state.movieShow1}
                    numColumns={4}
                    renderItem={({ item }) =>
                      <WingBlank size="sm">
                        <Button disabled={Date.now() > new Date(item.startDateTime) ? true:false} onPress={() => { this.goToBooking(item) }}>{this.addZero(new Date(item.startDateTime).getHours())}:
                  {this.addZero(new Date(item.startDateTime).getMinutes())}
                        </Button>
                        <WhiteSpace />
                      </WingBlank>
                    }
                  />
                </View>
                <View style={{ flex: 5, alignItems: 'center' }}>
                  <WhiteSpace />
                  <FlatList
                    data={this.state.movieShow2}
                    numColumns={4}
                    renderItem={({ item }) =>
                      <WingBlank size="sm">
                        <Button disabled={Date.now() > new Date(item.startDateTime) ? true:false} onPress={() => { this.goToBooking(item) }}>{this.addZero(new Date(item.startDateTime).getHours())}:
                  {this.addZero(new Date(item.startDateTime).getMinutes())}
                        </Button>
                        <WhiteSpace />
                      </WingBlank>
                    }
                  />
                </View>
                <View style={{ flex: 5, alignItems: 'center' }}>
                  <WhiteSpace />
                  <FlatList
                    data={this.state.movieShow3}
                    numColumns={4}
                    renderItem={({ item }) =>
                      <WingBlank size="sm">
                        <Button disabled={Date.now() > new Date(item.startDateTime) ? true:false} onPress={() => { this.goToBooking(item) }}>{this.addZero(new Date(item.startDateTime).getHours())}:
                  {this.addZero(new Date(item.startDateTime).getMinutes())}
                        </Button>
                        <WhiteSpace />
                      </WingBlank>
                    }
                  />
                </View>
              </Tabs>
            </View>
            <View>

            </View>
          </View>

        </Modal>
      </View>
    )
  }
}

export default connect(null, { push })(listMovie)