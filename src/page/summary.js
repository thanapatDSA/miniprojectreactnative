import React, { Component } from 'react';
import { Image, StyleSheet, Text, View, Alert, TextInput, TouchableOpacity, FlatList } from 'react-native';
import { connect } from 'react-redux'
import styles from '../utilities/styles'
import NavigationBar from 'react-native-navbar';
import { Button, Card } from '@ant-design/react-native';
import axios from 'axios'
var totalprice = 0
class summary extends Component {
  state = {
    movie: [],
    seats: [],
    total: 0
  }
  UNSAFE_componentWillMount() {
    this.setState({ movie: this.props.location.state.movie, seats: this.props.location.state.seats })
  }
  goToMenu = () => {
    const { profile } = this.props
    axios({
      method: 'post',
      url: 'https://zenon.onthewifi.com/ticGo/movies/book',
      headers: { 'Authorization': `bearer ${this.props.profile[profile.length - 1].token}` },
      data: {
        showtimeId: this.state.movie._id,
        seats: this.state.seats
      }
    })
      .then(() => {
        this.props.history.push('./menu', { index: 1 })
        alert('Booking is Success')
        totalprice = 0
      })
      .catch((error) => {
        console.log(error);
        alert(error)
      })
  }

  price = (item) => {
    if (item === 'SOFA') {
      totalprice = totalprice + 500
      this.setState({ total: this.state.total + 500 })
      return 500
    } else if (item === 'PREMIUM') {
      totalprice = totalprice + 190
      this.setState({ total: this.state.total + 190 })
      return 190
    } else {
      totalprice = totalprice + 150
      this.setState({ total: this.state.total + 150 })
      return 150
    }
  }

  render() {
    const { profile } = this.props
    console.log('showTime Movie:', this.state.movie);
    console.log('seats:', this.state.seats);
    console.log('token:', this.props.profile[profile.length - 1].token);
    console.log('movie id :', this.state.movie._id);


    return (
      <View style={styles.container}>
        <View style={styles.header} >
          <View style={{ flex: 1, }}>
            <NavigationBar title={{ title: 'Summary' }}
              leftButton={{
                title: 'Back',
                handler: () => this.props.history.push('./seatSelect', { movie: this.state.movie },totalprice = 0)
              }} />
          </View>
        </View>
        <View style={{ flex: 11, alignItems: 'center' }}>
          <FlatList
            data={this.state.seats}
            renderItem={({ item }) =>
              <Card>
                <Card.Header
                  title='Ticket'
                />
                <Card.Body>
                  <View style={{ flexDirection: 'row' }}>
                    <View>
                      <Text style={{ fontSize: 16 }}> SeatType:{item.type} </Text>
                      <Text style={{ fontSize: 20 }}> R:{item.row} C:{item.column} </Text>
                    </View>
                    <View style={{ alignItems: 'center' }}>
                      <Text style={{ fontSize: 25 }}> Price: {this.price(item.type)} THB </Text>
                    </View>
                  </View>
                </Card.Body>
              </Card>
            } />
          <Text style={{ fontSize: 30 }}>Total : {totalprice} THB</Text>
        </View>
        <View>
          <Button type='primary' onPress={() => { 
            Alert.alert(
              'Alert ',
              'Are you sure to buy ticket',
              [
                {
                  text: 'Cancel',
                  onPress: () => console.log('Cancel Pressed'),
                  style: 'cancel',
                },
                {text: 'OK', onPress: () => this.goToMenu()},
              ],
              {cancelable: false},
            )
             }}>Confirm</Button>
        </View>
      </View>
    )
  }
}
const mapStateToProps = (state) => {
  return {
    profile: state.profile
  }
}
export default connect(mapStateToProps)(summary)