import React, { Component } from 'react';
import { BottomNavigation, DefaultTheme } from 'react-native-paper';


import ListMovie from './listMovie'
import HistoryTicket from './historyTicket'
import Profile from './profile'


const ListMovieRoute = () => <ListMovie />
const HistoryTicketRoute = () => <HistoryTicket />
const ProfileRoute = () => <Profile />

class menu extends Component {
  
  state = {
    index: 0,
    routes: [
      { key: 'movie', title: 'Movie', icon: 'movie-filter', color: '#f1c40f' },
      { key: 'ticket', title: 'Ticket', icon: 'local-movies', color: '#3498db' },
      { key: 'profile', title: 'Profile', icon: 'person', color: '#f1c40f' },
    ],
  }
  UNSAFE_componentWillMount() {
    if (this.props.location.state.index !== 0) {
      this.setState({ index: this.props.location.state.index  })
    } 

  }
  _handleIndexChange = index => this.setState({ index });
  _renderScene = ({ route, jumpTo }) => {
    switch (route.key) {
      case 'movie':
        return <ListMovieRoute jumpTo={jumpTo} />
      case 'ticket':
        return <HistoryTicketRoute jumpTo={jumpTo} />
      case 'profile':
        return <ProfileRoute jumpTo={jumpTo} history={this.props.history} />
    }
  }

  render() {
    return (
      <BottomNavigation
        navigationState={this.state}
        onIndexChange={this._handleIndexChange}
        renderScene={this._renderScene}
        theme={theme}
      />

    )
  }
}
const theme = {
  ...DefaultTheme,
  roundness: 2,
  colors: {
    ...DefaultTheme.colors,
    primary: '#148ee9',
    accent: '#f1c40f',
  }
};

export default menu