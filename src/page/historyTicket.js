import React, { Component } from 'react';
import { Image, StyleSheet, Text, View, Alert, TextInput, TouchableOpacity, FlatList } from 'react-native';
import { connect } from 'react-redux'
import NavigationBar from 'react-native-navbar';
import { NavBar, Icon, Card, Button, WingBlank, WhiteSpace, Tabs } from '@ant-design/react-native';
import styles from '../utilities/styles'
import axios from 'axios'
class historyTicket extends Component {
  state = {
    tickets: [],
    ticket: []
  }
  UNSAFE_componentWillMount() {
    const { profile } = this.props
    axios.get('https://zenon.onthewifi.com/ticGo/movies/book', {
      headers: { 'Authorization': `bearer ${this.props.profile[profile.length - 1].token}` }
    })
      .then(res => {
        console.log('RES:', res.data);
        const dataTickets = res.data
        this.setState({ tickets: dataTickets })
        for (let index = 0; index < dataTickets.length; index++) {
          console.log('Ticket INdex?:', dataTickets[index]);
          axios.get(`https://zenon.onthewifi.com/ticGo/movies/showtimes/${dataTickets[index].showtime}`)
            .then(res => {
              this.setState({ ticket: [...this.state.ticket, res.data] })
              console.log('ticket:', this.state.ticket);
              console.log('ticketS:', this.state.tickets);
            })
            .catch(error => {
              console.log('ERROR_showtime:', error);
            })
        }
      })
      .catch(error => {
        console.log('ERROR:', error);
      })
  }
  seatPrint = (seats) => {
    const seat = []
    for (let index = 0; index < seats.length; index++) {
      seat.push(<Text>{seats[index].type} C: {seats[index].column} R: {seats[index].row}</Text>)
    }
    return seat
  }
  addZero(time) {
    if (time < 10) {
      time = "0" + time;
    }
    return time;
  }
  render() {
    return (
      <View style={styles.container}>
        <View style={{ flex: 0.8 }}>
          <NavigationBar
            title={{
              title: 'Ticket',
            }}
          />
        </View>
        <View style={{ flex: 11 }}>
          <FlatList
            inverted data={this.state.ticket}
            renderItem={({ item, index }) =>
              <Card>
                <Card.Header
                  title={item.movie.name}
                  extra={item.movie.duration + ' min'}
                />
                <Card.Body>
                  <View style={{ flexDirection: 'row' }}>
                    <View>
                      <WingBlank>
                        <TouchableOpacity>
                          <Image style={{ width: 150, height: 222 }}
                            source={{ uri: item.movie.image }} />
                        </TouchableOpacity>
                      </WingBlank>
                    </View>
                    <View>
                      <Text>{new Date(this.state.tickets[index].createdDateTime).toLocaleDateString()}</Text>
                      {this.seatPrint(this.state.tickets[index].seats)}
                      <Text> Show time : {this.addZero(new Date(item.startDateTime).toLocaleDateString())} </Text>
                      <Text> {this.addZero(new Date(item.startDateTime).getHours())} : {this.addZero(new Date(item.startDateTime).getMinutes())}</Text>
                    </View>
                  </View>
                </Card.Body>
                <Card.Footer
                  content="Soundtrack / Subtitle"
                  extra={item.movie.soundtracks + '/' + item.movie.subtitles}
                />
              </Card>
            }

          />
        </View>

      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    profile: state.profile
  }
}
export default connect(mapStateToProps)(historyTicket)