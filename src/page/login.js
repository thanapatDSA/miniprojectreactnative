import React, { Component } from 'react';
import { Image, StyleSheet, Text, View, Alert, TextInput, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux'
import { NavBar, Icon, Card, Button, InputItem, WhiteSpace } from '@ant-design/react-native';
import styles from '../utilities/styles'
import axios from 'axios'
class login extends Component {
  state = {
    email: '',
    password: '',
    pic: 'https://puu.sh/CQUJ1/ac37c354a6.png',
  }
  componentWillMount = () => {
    // this.props.addProfile('poom@poom.poom', '1234', 'Thanapat', 'Panyo')
  }

  goToMenu = (email, password) => {
    console.log('email:', email, ' password:', password);
    axios({
      url: 'https://zenon.onthewifi.com/ticGo/users/login?email',
      method: 'post',
      data: {
        email: email,
        password: password,
      }
    }).then(res => {
      const { data } = res
      const { user } = data
      // console.log("DATA:",data.user.email);
      this.props.addProfile(data.user.email, data.user.password, data.user.firstName, data.user.lastName, data.user.token);
      this.props.history.push('/menu', { index: 0 })
    }).catch(e => {
      if (e.response.data.errors.email === "Email can't be blank") {
        alert(e.response.data.errors.email)
      } else if (e.response.data.errors.password === "Password can't be blank") {
        alert(e.response.data.errors.password)
      } else if (e.response.data.errors.password === "is invalid") {
        alert('Password is invalid')
      }
      console.log("error " + e.response.data.errors.password)
    })
  }

  goToMenuTest = () => {
    axios({
      url: 'https://zenon.onthewifi.com/ticGo/users/login?email',
      method: 'post',
      data: {
        email: 'in-ni-in@hotmail.com',
        password: '3th6u8sw',
      }
    }).then(res => {
      const { data } = res
      const { user } = data
      // console.log("DATA:",data.user.email);
      this.props.addProfile(data.user.email, data.user.password, data.user.firstName, data.user.lastName, data.user.token);
      this.props.history.push('/menu', { index: 0 })
    }).catch(e => {
      if (e.response.data.errors.email === "Email can't be blank") {
        alert(e.response.data.errors.email)
      } else if (e.response.data.errors.password === "Password can't be blank") {
        alert(e.response.data.errors.password)
      } else if (e.response.data.errors.password === "is invalid") {
        alert('Password is invalid')
      }
      console.log("error " + e.response.data.errors.password)
    })
  }
  goToReg = () => {
    this.props.history.push('/register')
  }
  render() {
    const { profile } = this.props
    console.log('user:', profile);
    return (
      <View style={styles.container}>
        <View style={{ flex: 1 }} />
        <View style={{ flex: 6, alignItems: 'center' }}>
          <Image style={{ width: 250, height: 250, borderRadius: 250 / 2, backgroundColor: '#000' }}
            source={{ uri: this.state.pic }} />
          <View >
            <WhiteSpace />
            <InputItem placeholder='E-mail'
              clear
              labelNumber={2}
              type="primary"
              style={{ height: 40 }}
              onChangeText={(value) => { this.setState({ email: value }) }} ><Icon name={'mail'} /></InputItem>
            <InputItem placeholder='Password'
              clear
              labelNumber={2}
              type="password"
              style={{ height: 40 }}
              onChangeText={(value) => { this.setState({ password: value }) }} ><Icon name={'lock'} /></InputItem>
            <WhiteSpace />
            <Button onPress={() => { this.goToMenu(this.state.email, this.state.password) }}
              type="primary"
              style={{ width: 300, height: 40 }} >Login</Button>
            <WhiteSpace />
            <Button onPress={() => { this.goToReg() }}
              type="primary"
              style={{ width: 300, height: 40 }} >Register</Button>

          </View>

        </View>
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    profile: state.profile
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    addProfile: (email, password, firstname, lastname, token) => {
      dispatch({
        type: 'ADD_PROFILE',
        email: email,
        password: password,
        firstname: firstname,
        lastname: lastname,
        token: token
      })
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(login)